<!-- .slide: data-background-image="./files/wallstreet.jpg" data-background-opacity="0.5"-->
# Einzelunternehmung <!-- .element: class="r-fit-text" -->

Präsentation von Raphael Werner

---

### Was ist ein Einzelunternehmen?

- beliebteste Rechtsform in Deutschland <!-- .element: class="fragment" -->
- Unterteilung nach: <!-- .element: class="fragment" -->
    - Einzelne natürliche Personen, die einer selbständigen Tätigkeit nachgehen, z.B. Freiberufler, Kleingewerbetreibende sowie Land- und Forstwirte
    - Unternehmen eines eingetragenes Kauf-mannes/-frau im Sinne des Handelsgesetzbuches

note:

- beliebteste Rechtsform in Deutschland mit über 2 Mio Einzelunternehmern
- Rechtsform:  
  Gibt den rechtlichen Rahmen eines Unternehmens vor.
- Unterteilbar nach: 
    - Einzelne natürliche Personen, die einer selbständigen Tätigkeit nachgehen, z.B. Freiberufler, Kleingewerbetreibende sowie Land- und Forstwirte
    - Unternehmen eines eingetragenes Kauf-mannes/-frau im Sinne des Handelsgesetzbuches
- Gewerbetreibender bzw. Inhaber ist nur **1 Person** (es können aber Personen angestellt werden)

---
<!-- .slide: data-background-image="./files/money.jpg" data-background-opacity="0.3"-->

### Gründung  & Startkapital eines Einzelunternehmens

- 1 Person <!-- .element: class="fragment" -->
- Kein Startkapital notwendig <!-- .element: class="fragment" -->

note:

- Nicht einheitlich, unterschied zwischen Kann-Kaufmann und Ist-Kaufmann
- 1 Person
- Kein Startkapital notwendig:  
    Freiberufler benötigen nur Steuernummer
    Gewerbetreibende Kaufmänner müssen sich beim Handelsregister eintragen (Abteilung A) (Kosten zwischen 10 - 60€)

Handelsregister:

| Abteilung A | Abteilung B |
| ----------- | ----------- |
|Personengesellschaften | Kapitalgesellschaften |
| Einzelunternehmen | |

---

### Namensfindung

- Vor- und Nachname + Branchenname <!-- .element: class="fragment" --> 
    - z.B. Blumenhandel Max Mustermann
- Phantasiename bei gewerbetreibendem Kaufmann möglich <!-- .element: class="fragment" -->
- e.K. <!-- .element: class="fragment" -->

note:

- Meist der Vor- und Nachname
- ergänzbar mit Branchenname
- gewerbetreibender Kaufmann darf Phantasienamen nutzen (Kürzel e.K. = eingetragener Kauf-mann /-frau)

---
<!-- .slide: data-background-image="./files/prison.jpg" data-background-opacity="0.5"-->

### Haftung 

- Alleinige Haftung <!-- .element: class="fragment" -->
- mit Geschäfts- und Privatvermögen <!-- .element: class="fragment" -->

note:

- Alleinige Haftung
- Er haftet mit Geschäfts und Privatvermögen -> großer Nachteil

---
<!-- .slide: data-background-image="./files/boerse.jpg" data-background-opacity="0.5" -->

### Gewinn & Verlust

- Inhaber erhält Gewinn und trägt Verlust zu 100% selber <!-- .element: class="fragment" -->

note:

- Inhaber ist zu 100% am Gewinn beteiligt - wesentlicher Vorteil
- Inhaber trägt auch den Verlust auch zu 100%

---

### Pro & Kontra

| Vorteile | Nachteile |
| -------- | --------- |
| Gewinne gehören uneingeschränkt dem Inhaber | uneingeschränkte Haftung |
| Inhaber kann alleine Entscheidungen treffen | Buchführungspflichten (nur als Kaufmann) |
| Kein Startkapital notwendig | "Einzelkämpfer" |
| Hohe Kreditwürdigkeit | |

---
<!-- .slide: style="font-size: 0.5em" -->
## Quellen

- Einzelunternehmen
  [https://www.existenzgruender.de/DE/Gruendung-vorbereiten/Rechtsformen/Einzelunternehmen/inhalt.html](https://www.existenzgruender.de/DE/Gruendung-vorbereiten/Rechtsformen/Einzelunternehmen/inhalt.html) (Abgerufen 01.04.23)
- Kirschgens, Sabine: Einzelunternehmen IHK  
  [https://www.ihk.de/berlin/service-und-beratung/recht-und-steuern/firma-und-rechtsformen/rechtsformen-fuer-unternehmen/einzelunternehmen-2253550](https://www.ihk.de/berlin/service-und-beratung/recht-und-steuern/firma-und-rechtsformen/rechtsformen-fuer-unternehmen/einzelunternehmen-2253550)  (Abgerufen 01.04.23)
- explainity ® Erklärvideos: Unternehmensrechtsformen Teil 1: Die Einzelunternehmung einfach erklärt (explainity® Erklärvideo)  
  [https://www.youtube.com/watch?v=93X0y6c5HNE](https://www.youtube.com/watch?v=93X0y6c5HNE) (Abgerufen 01.04.23)
- Herr Gerold: Einzelunternehmen  
  [https://www.youtube.com/watch?v=mLBLpFITUFQ](https://www.youtube.com/watch?v=mLBLpFITUFQ) (Abgerufen 01.04.23)
- lo lo: (Unsplash)  
  [https://unsplash.com/de/fotos/CeVj8lPBJSc](https://unsplash.com/de/fotos/CeVj8lPBJSc)
- Alexander Grey: (Unsplash)  
  [https://unsplash.com/de/fotos/8lnbXtxFGZw](https://unsplash.com/de/fotos/8lnbXtxFGZw)
- Marco Chilese: (Unsplash)
  [https://unsplash.com/de/fotos/2sMbKyQvom4](https://unsplash.com/de/fotos/2sMbKyQvom4)
- Nick Chong: (Unsplash)  
  [https://unsplash.com/de/fotos/N__BnvQ_w18](https://unsplash.com/de/fotos/N__BnvQ_w18)
- James Corden Hello GIF by The Late Late Show with James Corden:  
  [https://media.giphy.com/media/l2R0eYcNq9rJUsVAA/giphy.gif](https://media.giphy.com/media/l2R0eYcNq9rJUsVAA/giphy.gif) 

---
<!-- .slide: data-background-image="https://media.giphy.com/media/l2R0eYcNq9rJUsVAA/giphy.gif" data-background-opacity="0.5" -->
## Vielen Dank! <!-- .element: class="r-fit-text" -->

---

## Links

Die Folien findet ihr unter:  
[praesentation.e3fs4.schule](https://praesentation.e3fs4.schule/)  

Den Source Code findet ihr unter:  
[https://gitlab.com/re3fs4/praesentation](https://gitlab.com/re3fs4/praesentation)  

Das Handout findet ihr auf Nextcloud oder unter:  
[https://ap.e3fs4.schule/wi/rechtsformen/](https://ap.e3fs4.schule/wi/rechtsformen/)

Diese Präsentation wurde mit [reveal.js](https://revealjs.com/) erstellt.